const passport = require('passport');


// const userAuth  =  async (req, res, next) => {passport.authenticate("jwt",{session:false},(err,user,info) => {

//         if (err || !user) {
//                 return res.status(401).json({
//                         message:"Please check the token",
//                         success:false
//                 });
//         }else{
            
//                 return next(); 

//         }
    
// })(req, res, next)};



const userAuth  = passport.authenticate("jwt",{session:false});


/*
  ตรวจสอบ role ตรงกับเงื่อนไข role ที่กำหนดหรือเปล่า  
*/
const validateRole = (role) => (req,res,next) =>{

         console.log(role);
        /* ตรวจสอบว่า role ที่ส่งเข้ามา ตรงกับ passport ที่ใช้ยืนยันหรือเปล่า */
        if(role.includes(req.user.type)){

                  return next();
        }
    
        return res.status(401).json({

                message:"Unauthorized",
                success:false
            
        });

};


module.exports = {validateRole,userAuth}