const modelUser = require('../models/users');
const {Strategy,ExtractJwt}   =  require('passport-jwt');
const {SECRET}    =  require('../config/db');


const jwtOptions = {

        jwtFromRequest:ExtractJwt.fromAuthHeaderAsBearerToken(),
        secretOrKey:SECRET

};


module.exports  = passport => {

                passport.use(
                    new Strategy(jwtOptions,async(payload,done)=>{

                            await modelUser.findByPk(payload.id).then(user =>{
                                
                                    if(user){
                                        
                                        return  done(null,user);
                                    }

                                    return done(null,false);

                            }).catch(err =>{

                                    return done(null,false);

                            });
            }));

};




