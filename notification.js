const  SSE = require('sse')
, http = require('http');
const connect = require("./config/connect");

var data = ""
var server = http.createServer(function(req, res) {
 res.writeHead(200, {
  'Content-Type': 'text/event-stream',
  'Access-Control-Allow-Origin': '*'
 });

 setInterval(  async function() {
          data =  await connect.query("call  api_getAlarmUnclear()");
          if(data.length != 0){
            msg = "id: msg1\ndata: yes\n\n";
         
          }else{
          
            msg = "id: msg2\ndata: no\n\n";
          }
    
          res.write(msg);
         
 }, 20000);

});

server.listen(9000, '172.16.12.40', function() {
  var sse = new SSE(server);
  sse.on('connection', function(client) {
    client.send('hi there!');
  });
});