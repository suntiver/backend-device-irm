const  cors = require("cors");
const express =  require("express");
const bodyParser  = require("body-parser");
const  {success,error}  = require("consola");
const passport = require('passport');
const {PORTRUN} = require('./config/db');
const app  = express();



app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(passport.initialize());
require('./middlewares/passport')(passport);


app.use('/api',require('./routes/api'));


app.listen(PORTRUN,() =>

        success({message:`Server started on PORT  ${PORTRUN}`,badge:true})  
);




