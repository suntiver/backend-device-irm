const route             = require('express').Router();
const {register}        = require('../controllers/Auth/RegisterController');
const {login}           = require('../controllers/Auth/LoginController');
const {validateRole,userAuth}    = require('../middlewares/validate');
const deviceController  = require('../controllers/admin/deviceController');
const dataController  = require('../controllers/user/dataController');


/*
    API สำหรับ ผู้ใช้ทั่วไป
*/
route.post("/register",async(req,res)=>{ await  register( req.body,"User",res)});
route.post("/login-user",async(req,res)=>{ await  login( req.body,"User",res)});
route.get("/electric-current",userAuth,validateRole(['Admin','User']),dataController.getElectricCurrentAll);
route.get("/current-all",userAuth,validateRole(['Admin','User']),dataController.getCurrentAll);
route.get("/report-device",userAuth,validateRole(['Admin','User']),dataController.reportDevice);
route.get("/report-rack",userAuth,validateRole(['Admin','User']),dataController.reportRack);
route.get("/report-group",userAuth,validateRole(['Admin','User']),dataController.reportGroup);
route.get("/alarm-unclear",userAuth,validateRole(['Admin','User']),dataController.currentAlarmUnclear);


/*
    API สำหรับ ผู้ดูเเลสูงสุด
*/
route.post("/login-admin",async(req,res)=>{ await  login( req.body,"Admin",res)});
route.get("/getDeviceConnectNew",userAuth,validateRole(['Admin']),deviceController.getDeviceConnectNew);
route.get("/getDeviceConnectOpen",userAuth,validateRole(['Admin']),deviceController.getDeviceConnectOpen);
route.get("/getDeviceConnectClose",userAuth,validateRole(['Admin']),deviceController.getDeviceConnectClose);
route.get("/getRacks",userAuth,validateRole(['Admin']),deviceController.getRacks);

module.exports = route;