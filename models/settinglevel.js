const Sequelize  =  require('sequelize');
const connect     = require('../config/connect');





const settinglevel = connect.define(
        "settinglevel",
        {
                id_settingLevel:{
                        type:Sequelize.BIGINT(11),
                        autoIncrement:true,
                        allowNull:false,
                        primaryKey:true
                },
                level:{
                        type:Sequelize.STRING(10),
                        defaultValue:"Normal",
                        allowNull:false
                },
                datetime:{
                        type:Sequelize.DATE,
                        allowNull:true
                    
                }
        },
        {
          //option
        }
      );
      
      (async () => {
        await settinglevel.sync({ force: false });
      })();

module.exports  = settinglevel;