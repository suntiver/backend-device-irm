const Sequelize  =  require('sequelize');
const connect     = require('../config/connect');


const users =  connect.define('users',{
            
                id_user :{
                        type:Sequelize.BIGINT(20),
                        autoIncrement:true,
                        allowNull:false,
                        primaryKey:true
                },
                email:{
                        type:Sequelize.STRING(100),
                        allowNull:false
                },
                password:{
                        type:Sequelize.STRING(255),
                        allowNull:false
                },
                remember_token:{
                        type:Sequelize.STRING(255),
                        allowNull:true
                },
                type:{
                        type:Sequelize.STRING(30),
                        defaultValue:"User",
                        allowNull:false
                },
                status:{
                        type:Sequelize.STRING(30),
                        defaultValue:"Wait",
                        allowNull:false
                },
                datetime:{
                        type:Sequelize.DATE,
                        allowNull:true
                    
                }
})


module.exports  = users;
