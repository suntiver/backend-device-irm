
const  Sequelize  = require('sequelize');
const  {DB,USER,PASS,DIALECT,HOST}  = require('../config/db');

/*  
สร้าง การติดต่อ DB  ผ่าน Sequelize เเทนการติดต่อเเบบเดิม ที่่ต้องเขียน sql
*/

const sequelize  = new Sequelize(
    DB,
    USER,
    PASS,
    {
        dialect:DIALECT,
        host:HOST
    }

);

module.exports  = sequelize;