const env  = require('dotenv').config();


module.exports = {
        SECRET:process.env.APP_SECRET,
        DB    :process.env.APP_DB,
        USER  :process.env.APP_USER,
        PASS :process.env.APP_PASS,
        DIALECT :process.env.APP_DIALECT,
        HOST  :process.env.APP_HOST,
        PORTRUN:process.env.APP_PORTRUN
}