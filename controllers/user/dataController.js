const connect = require("../../config/connect");

/*
    เป็นฟังชันสำหรับร้องขอข้อมูลอุปกรณ์วัดกระเเสไฟที่เปิดใช้งานเเล้วทั้งหมด
*/
exports.getElectricCurrentAll = async (req, res) => {
  const data = await connect.query("call api_getElectricCurrentAll()");

  return res.status(200).json({
    data,
    status: true,
  });
};

/*
    เป็นฟังชันสำหรับร้องขอข้อมูลอลามที่เกิดขึ้นทั้งหมด
*/
exports.getCurrentAll = async (req, res) => {
  const data = await connect.query("call  api_getCurrentAll()");

  return res.status(200).json({
    data,
    status: true,
  });
};

/*
    เป็นฟังชันสำหรับดึง report ตามอุปกรณ์
*/
exports.reportDevice = async (req, res) => {
  const data = await connect.query(
    "call  api_getReportDeviceAll(:startDate,:endDate,:day)",
    {
      replacements: {
        startDate: req.query.startDate,
        endDate: req.query.endDate,
        day: req.query.day,
      },
    }
  );
  return res.status(200).json({
    data,
    status: true,
  });
};


/*
    เป็นฟังชันสำหรับดึง report ตาม Rack
*/
exports.reportRack = async (req, res) => {
  const data = await connect.query(
    "call  api_getReportRackAll(:startDate,:endDate,:day)",
    {
      replacements: {
        startDate: req.query.startDate,
        endDate: req.query.endDate,
        day: req.query.day,
      },
    }
  );
  return res.status(200).json({
    data,
    status: true,
  });
};



/*
    เป็นฟังชันสำหรับดึง report ตาม Group
*/
exports.reportGroup = async (req, res) => {
  const data = await connect.query(
    "call  api_getReportGroupAll(:startDate,:endDate,:day)",
    {
      replacements: {
        startDate: req.query.startDate,
        endDate: req.query.endDate,
        day: req.query.day,
      },
    }
  );
  return res.status(200).json({
    data,
    status: true,
  });
};




/*
    เป็นฟังชันสำหรับดึงอลามที่ยังไม่เคลีย
*/
exports.currentAlarmUnclear = async (req, res) => {
  const data = await connect.query("call  api_getAlarmUnclear()");
 
  return res.status(200).json({
    data,
    status: true,
  });
};




