const connect     = require('../../config/connect');
const setting     = require('../../models/settinglevel');

/*
    เป็นฟังชันสำหรับร้องขอข้อมูลอุปกรณ์ที่เชื่อมต่อเข้ามาใหม่
*/
exports.getDeviceConnectNew  = async (req,res) => {

        const data = await connect.query("call api_getDeviceConnectNew()");

        return res.status(200).json({
            data,
            status:true
        });

}

/*
    เป็นฟังชันสำหรับร้องขอข้อมูลอุปกรณ์ที่เปิดใช้งาน
*/
exports.getDeviceConnectOpen  = async (req,res) => {

    const data = await connect.query("call  api_getDeviceConnectOpen()");

    return res.status(200).json({
        data,
        status:true
    });

}

/*
    เป็นฟังชันสำหรับร้องขอข้อมูลอุปกรณ์ที่ปิดใช้งาน
*/
exports.getDeviceConnectClose  = async (req,res) => {

    const data = await connect.query("call  api_getDeviceConnectClose()");

    return res.status(200).json({
        data,
        status:true
    });

}

/* 
    เป็นฟังก์ชันสำหรับร้องขอข้อมูลตู้เเร็คทั้งหมด
*/
exports.getRacks  = async (req,res) => {

    // const data   =  await setting.findAll();
    const data = await connect.query("call  api_getRacks()");
    console.log(JSON.stringify(data));
    return res.status(200).json({
        data,
        status:true
    });

}
