const  bcrypt =  require('bcryptjs');
const modelUser = require('../../models/users');

const register = async(req,role,res) => {

    try {
             const emailTaken  = await validateEmail(req.email);
                       
             if(!emailTaken){


                return  res.status(400).json({
                     message:`Email  is already  registered.`,
                     status:false
                });
              
                        
              
                
             }

             const password  = await  bcrypt.hash(req.password,12);
             await modelUser.create({...req,password,role});

             return res.status(200).json({
                    message:"Now you are successfully registered.Please login",
                    status:true

             });

    } catch (error) {

            return res.status(500).json({

                message:"Unable to create your account.",
                status:false

            });
            
    }

}


const validateEmail = async (emailaddress) =>{

    const email =   await  modelUser.findOne({
        where: { email:emailaddress  }

    });
  
   
    return  email ? false:true;

};

module.exports = { register};