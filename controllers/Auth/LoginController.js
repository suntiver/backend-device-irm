const  bcrypt =  require('bcryptjs');
const  jwt = require('jsonwebtoken');
const { SECRET } = require('../../config/db');
const modelUser = require('../../models/users');


const  login = async(req,role,res) =>{


        let {email,password}  = req;
        const userCheck   = await modelUser.findOne({   where:{email:email}});

       
        if(!userCheck){
                return res.status(404).json({

                    message:"Username is not found.Invalid login creadentials.",
                    status:false

                });
        }


        if(userCheck.type != role){
        
            return res.status(403).json({
                    
              message:"Please make sure you are logging in from the right portal.",
              status:false

            });

         }

            
        let isMatch  = await  bcrypt.compare(password,userCheck.password);
         
        if(isMatch){

            if(userCheck.status == "Wait"){
                
                return res.status(403).json({
                        message:"Email not approved",
                        status:false
                });

            }else{
                
                let token = jwt.sign({
                    id:userCheck.id_user,
                    role:userCheck.type,
                    username:userCheck.username,
                },SECRET,{expiresIn:"1 Days"});

                await modelUser.update({
                        remember_token: `Bearer ${token}`
                },{
                        where:{
                            id_user:userCheck.id_user
                        }
                });
            
                return res.status(200).json({
                        username:userCheck.username,
                        email:userCheck.email,
                        token:`Bearer ${token}`,
                        status:true

                }); 
            }
        }else{
           
                return res.status(403).json({     
                    message:"Incorrect  password.",
                    status:false
                });

        }

}





module.exports = { login};